import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {

  public get(field: string): string {
    return localStorage.getItem(field)
  }
  public set(field: string, value: string) {
    localStorage.setItem(field, value);
  }
  public contains(field: string): boolean {
    return !!localStorage.getItem(field);
  }
}
