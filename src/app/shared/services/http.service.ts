import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppStateService } from './appstate.service';

@Injectable()
export class HttpService {
  protected _path = 'https://cloud-api.yandex.net/v1';

  get headers() {
    return new HttpHeaders()
      .append('Content-Type', 'application/json')
      .append('Authorization', `OAuth ${this.appState.token}`)
      .append('Accept', 'application/json');
  }

  protected get(url: string, params?: any): Observable<any> {
    return this.http.get(`${this._path}/${url}`, { params, headers: this.headers })
  }

  protected post(url: string, data: any): Observable<any> {
    return this.http.post(`${this._path}/${url}`, data, { headers: this.headers })
  }

  constructor(readonly http: HttpClient, readonly appState: AppStateService) { }
}
