import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpService } from './http.service';
import { AppStateService } from './appstate.service';
import { ID } from '../models'

@Injectable()
export class AuthService extends HttpService {

  get login() {
    return `http://oauth.yandex.ru/authorize?response_type=token&client_id=${ID}&display=popup`
  }

  constructor(http: HttpClient, state: AppStateService) {
    super(http, state);
    this._path = 'https://cors-anywhere.herokuapp.com/http://oauth.yandex.ru';
  };
}
