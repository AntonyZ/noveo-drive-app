import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AppStateService } from './appstate.service';

@Injectable()
export class AuthGuard implements CanActivate
{
  constructor(readonly appState: AppStateService, readonly router: Router) {}

  canActivate(router: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    if (!this.appState.loggedin) {
      this.router.navigateByUrl('/login');
    }
    return of(this.appState.loggedin);
  }
}
