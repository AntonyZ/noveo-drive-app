import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpService } from './http.service';
import { AppStateService } from './appstate.service';

@Injectable()
export class ApiService extends HttpService {

  public info() {
    return this.get('disk')
  }

  public resources() {
    return this.get('disk/resources');
  }

  constructor(http: HttpClient, state: AppStateService) { super(http, state) };
}
