import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';

@Injectable()
export class AppStateService {
  private _loggedin = false;

  get loggedin() {
    return this._loggedin;
  }
  get token() {
    return this.storage.get('token');
  }
  set token(token) {
    this.storage.set('token', token);
    this._loggedin = true;
  }

  constructor(readonly storage: StorageService) {
    this._loggedin = storage.contains('token');
  }
}
