import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

import { Resolve, Router } from '@angular/router';

import { ActivatedRouteSnapshot } from '@angular/router';
import { AppStateService } from './appstate.service';
import { catchError } from 'rxjs/operators';

@Injectable()
export class TokenResolver implements Resolve<any> {
  constructor(readonly appState: AppStateService, readonly apiService: ApiService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot) {
    const hash = document.location.hash;
    if (hash !== '') {
      this.appState.token = /access_token=([^&]+)/.exec(hash)[1];
    }

    return this.apiService.info()
      .pipe(
        catchError(() => this.router.navigate(['/login'], {replaceUrl: true}))
      );
  }
}
