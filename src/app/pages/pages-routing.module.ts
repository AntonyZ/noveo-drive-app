import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import * as fromPages from './index';
import { TokenResolver } from '../shared/services';

const routes: Routes = [
  {
    path: 'disk',
    resolve: [TokenResolver],
    children: [
      { path: '', component: fromPages.ListPageComponent },
      { path: ':path', component: fromPages.InnerPageComponent },
    ]
  },
  {
    path: 'login',
    component: fromPages.LoginPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
