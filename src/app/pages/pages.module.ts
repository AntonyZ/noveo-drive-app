import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PagesRoutingModule } from './pages-routing.module';
import { pages } from './index';
import { AuthPageComponent } from './auth-page/auth-page.component';

@NgModule({
  declarations: [...pages, AuthPageComponent],
  imports: [
    CommonModule,
    FormsModule,
    PagesRoutingModule
  ]
})
export class PagesModule { }
