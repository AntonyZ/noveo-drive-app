import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: './list-page.component.html',
  styleUrls: ['./list-page.component.scss']
})
export class ListPageComponent implements OnInit {

  constructor(route: ActivatedRoute) {
    const snapshot = route.snapshot;
    console.log(snapshot.data);
  }

  ngOnInit() {
  }

}
