import { LoginPageComponent } from './login-page/login-page.component'
import { InnerPageComponent } from './inner-page/inner-page.component'
import { ListPageComponent } from './list-page/list-page.component'
import { AuthPageComponent } from './auth-page/auth-page.component'

export * from './login-page/login-page.component'
export * from './inner-page/inner-page.component'
export * from './list-page/list-page.component'
export * from './auth-page/auth-page.component'

export const pages = [LoginPageComponent, InnerPageComponent, ListPageComponent, AuthPageComponent];
