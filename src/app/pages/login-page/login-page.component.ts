import { Component } from '@angular/core';
import { AuthService } from '../../shared/services/auth.service';

@Component({
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent {
  constructor(public auth: AuthService) { }
}
